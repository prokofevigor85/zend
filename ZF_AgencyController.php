<?php

/**
 * Controller for agencies.
 * 
 */
class Agency_AgencyController extends Agentstvo_Plugin_Action
{
    /**
     * Action of agency profile pages 
     * @param unknown $param
     */
    public function profileAction() {
        $agencyId   = (int)$this->getParam(3);
        $agencyData = Agentstvo_Query_Agency::getAgencyById($agencyId);
        $this->view->agencyOfficeData = Agentstvo_Query_Agency::getOfficeByAgencyId($agencyId);

        if ((int)$agencyData['id'] < 1 || $agencyData['url'] !== $this->getParam(1)) {
            header('Location: /404');
            exit;
        }

        $this->view->agency = new Agentstvo_Object($agencyData);

        $curUserId = Agentstvo_Auth::getUserId();
        $this->_prepareRegInfo($curUserId);
        $this->_prepareCurrentUserProfile($curUserId);

        $this->view->isStaff = Agentstvo_Query_Agency::isStaff($curUserId, $agencyId);

        $this->view->hideSideBar    = true;
        $this->view->showAgencyMenu = true;

        $this->view->uri = Agentstvo_Storage_Agency::agencyGetFilename($agencyId, $this->view->agency->photo);

        $this->view->services = Agentstvo_Query_Agency_Service::serviceGet($agencyId);

        $crUserSservices = Agentstvo_Query_User_Service::serviceGet($curUserId);
        $this->view->UserAndAgencySameService = false;
        foreach ($this->view->services as $agencyService) {
            foreach ($crUserSservices as $userService) {
                if ($agencyService['sys_service_types_id'] === $userService['sys_service_types_id']) {
                    $this->view->UserAndAgencySameService = true;
                    break;
                }
            }
        }
        
        
        // cancel bid
        if ($this->getParam('cancel_career')) {
            
            // remove bid
            $dataCareer = Agentstvo_Query_Agency_Service::careerContainsUser($agencyId,$curUserId);           
           
            Agentstvo_Query_Agency_Service::careerDelete($dataCareer['id']);
            
            // remove notification
            $notification_data = Agentstvo_Query_Agency_Service::notificationContains($dataCareer);
            
            Agentstvo_Query_Agency_Service::notificationIdDelete($notification_data['id']);           
            
        }

        // View about sent
        $this->view->careerContainsUser = false;
        if (Agentstvo_Query_Agency_Service::careerContainsUser($agencyId,$curUserId)) {
            $this->view->careerContainsUser = true;
        }
        
        $userSocialPagesService      = new Agency_Service_SocialUserPages();
        $this->view->showSocialPages = $userSocialPagesService->getPermissionsByAgency($agencyId);
        if (count($this->view->showSocialPages) > 0) {
            $this->view->socialConfig = $userSocialPagesService->getConfigs();
        }

        $this->view->showPhonesArray = explode(',', $this->view->agency->show_to_phones);
        $this->view->canShowPages    = $userSocialPagesService->canShowPages($this->view->showSocialPages);

        $this->_prepareMetaProfile();
    }

    /**
     * Action handles form of creating new agency. 
     */
    public function newagencyAction() {
      Agentstvo_Auth::rejectUnlessAuth();

      if ($this->isPost() === true) {
        exif_read_data();
          $result = $this->_processNewAgencyForm();

          if ($result !== false) {
              $this->_redirect($this->getRequest()->getParam('redirect_url'));
          }
      } else {
          $this->view->jQueryUI10 = true;
          $this->view->jQuery9    = true;

          $curUserId        = Agentstvo_Auth::getUserId();
          $user             = Agentstvo_Query_User::userGetFull($curUserId);
          $this->view->user = new Agentstvo_Object($user);

          $this->view->uri = Agentstvo_Storage_User::userGetFilename($curUserId, $this->view->user->photo);

          $meta = 'Регитрация агентства';

          $this->view->title       = $meta;
          $this->view->description = $meta;
          $this->view->keywords    = $meta;
      }
    }


    /**
     * Prepares data for database and runs query.
     * 
     * @return boolean
     */
    private function _processNewAgencyForm() {
   
        $name      = Daz_Request::get('name');
        $office_name = Daz_Request::get('office_name');
        $countryId = Daz_Request::get('counry');
        $regionId  = Daz_Request::get('region');
        $address   = Daz_Request::get('address');
        $email     = Daz_Request::get('email');
        $site      = Daz_Request::get('site', null);
        $site1     = Daz_Request::get('site_1', null);
        $skype     = Daz_Request::get('skype', null);
        $type     = Daz_Request::get('type');

        $phone[] = array (
            "prefix" => Daz_Request::get('phone_prefix1'),
            "code" => Daz_Request::get('phone_code1'),
            "num" => Daz_Request::get('phone_num1'),
            "dob" => Daz_Request::get('phone_dob1')
            );

        $phone[] = array (
            "prefix" => Daz_Request::get('phone_prefix2'),
            "code" => Daz_Request::get('phone_code2'),
            "num" => Daz_Request::get('phone_num2'),
            "dob" => Daz_Request::get('phone_dob2')
            );

        $phone[] = array (
            "prefix" => Daz_Request::get('phone_prefix3'),
            "code" => Daz_Request::get('phone_code3'),
            "num" => Daz_Request::get('phone_num3'),
            "dob" => Daz_Request::get('phone_dob3')
            );
        
        $phonesSerialized = serialize($phone);

        $result = Agentstvo_Query_Agency::createAgency(
            $name,
            $countryId,
            $regionId,
            $address,
            $phonesSerialized,
            $email,
            $site,
            $site1,
            $skype,
            $type,
            Agentstvo_Auth::getUserId(),
            $office_name
        );

        return true;
    }


    /**
     * Prepare fields for registration and login form.
     * 
     * @param integer $curUserId
     */
    private function _prepareRegInfo($curUserId) {
        $this->view->reg_error = array();

        $isAuth = isset($curUserId) === TRUE && (int)$curUserId > 0;
        if ($isAuth === FALSE) {
            $this->view->guest_user_view = true;
        }

        $this->view->reg = new User_Reg(true);
        $curUser         = Agentstvo_Auth::getUser();

        if ($this->getRequest()->isPost() === TRUE && (empty($curUser->passwd) === TRUE && $isAuth === TRUE)) {
            $this->view->reg->resetUser();
        }

         $this->view->userEmptyPasswd = trim($curUser->passwd) == '';
         $this->view->isUserBlocked   = $curUser->status === 'blocked';
    }


    /**
     * Prepares meta fields for a page. 
     */
    private function _prepareMetaProfile() {
        $meta = $this->view->agency->name;
        if (isset($this->view->agency->DATA['region']) === true && strlen(trim($this->view->agency->region)) > 0) {
            $meta .= ' ' . $this->view->agency->region;
        }

        $meta .= ' - Proagentov.ru';

        $this->view->title       = $meta;
        $this->view->description = $meta;
        $this->view->keywords    = $meta;
    }


    /**
     * Method creates current users's profile variable in the view.
     * 
     * @param unknown $curUserId
     */
    private function _prepareCurrentUserProfile($curUserId) {
      $curUser = Agentstvo_Query_User::userGetFull($curUserId);
        $this->view->curUserProfile = new Agentstvo_Object($curUser);

        $curUserAvaFilename     = Agentstvo_Query_User::userGetPhotoTmpName($curUserId);
        $this->view->curUserAva = Daz_String::merge(
                '/getthumbnail/[filename]/[user_id]',
                array('user_id' => $curUserId, 'filename' => $curUserAvaFilename)
        );
        
    }


    /**
     * Action shows thumbnail of an agency avatar.
     */
    public function getthumbnailAction() {
        $agencyId = $this->getParam('agencyId');
        $filename = $this->getParam('filename');

        $config   = Zend_Registry::get('configs');
        $fullpath = Daz_String::merge(
            $config->agency->thumbnail_filepath,
            array(
                'agencyId' => $agencyId,
                'filename' => $filename
            )
        );

        if (file_exists($fullpath) === FALSE) {
            $default = $config->agency->default_image;

            header('Content-Type: image/jpeg');
            echo file_get_contents($default);
        } else {
            $break = explode('.', $filename);
            $ext   = end($break);

            switch ($ext) {
              case 'jpg':
                  $type = "image/jpeg";
                  break;

              case 'jpeg':
                  $type = "image/jpeg";
                  break;

              case 'gif':
                  $type = "image/gif";
                  break;

              case 'png':
                  $type = "image/png";
                  break;

              default:
                  $type = false;
                  break;
            }

            if ($type === false || file_exists($fullpath) === false) {
                $default = $config->agency->default_image;

                header('Content-Type: image/jpeg');
                echo file_get_contents($default);
            } else {
                header('Content-Type: ' . $type);
                echo file_get_contents($fullpath);
            }
        }

        exit;
    }


    /**
     * Gets logo of an agency to show on ui.
     */
    public function getagencyimageAction() {
        $agencyId = $this->getParam('agencyId');
        $filename = $this->getParam('filename');

        $this->view->page    = 4;
        $this->view->subview = 2;

        $config = Zend_Registry::get('configs');

        $fullpath = Daz_String::merge(
            $config->agency->filepath,
            array(
                'agencyId' => $agencyId,
                'filename' => $filename
            )
        );

        if (file_exists($fullpath) === FALSE) {
            $default = $config->agency->default_image;

            header('Content-Type: image/jpeg');
            echo file_get_contents($default);
        } else {
            $break = explode('.', $filename);
            $ext   = end($break);

            switch (strtolower($ext)) {
              case 'jpg':
                  $type = "image/jpeg";
                  break;

              case 'jpeg':
                  $type = "image/jpeg";
                  break;

              case 'gif':
                  $type = "image/gif";
                  break;

              case 'png':
                  $type = "image/png";
                  break;

              default:
                  $type = false;
                  break;
            }

            if ($type === false) {
                $default = $config->agency->default_image;

                header('Content-Type: image/jpeg');
                echo file_get_contents($default);
            } else {
                header('Content-Type: ' . $type);
                echo file_get_contents($fullpath);
            }
        }

        exit;
    }


    /**
     * Prepares preview window for croping and saving logo of an agency.
     */
    public function editImageAction() {
        Agentstvo_Auth::rejectUnlessAuth();

        $this->view->page               = 4;
        $this->view->subview            = 2;
        $this->view->hideSideBar        = true;
        $this->view->showEditAgencyMenu = true;

        $curUserId = Agentstvo_Auth::getUserId();
        $this->_prepareCurrentUserProfile($curUserId);

        $agencyId = $this->getParam('agencyId');
        $filename = Agentstvo_Query_Agency::getTmpPhotoName($agencyId);

        $this->view->uri = Daz_String::merge(
            '/getimage/agency/[filename]/[agencyId]',
            array('agencyId' => $agencyId, 'filename' => $filename)
        );

        $agency = Agentstvo_Query_Agency::getAgencyById($agencyId);

        if ($agency['user_id'] != $curUserId) {
            header('Location: /404');
            exit;
        }

        $curUserId = Agentstvo_Auth::getUserId();
        $user = Agentstvo_Query_User::userGetFull($curUserId);
        $this->view->curUserProfile = new Agentstvo_Object($user);
        $this->view->curUserAva = Agentstvo_Storage_User::userGetFilename($curUserId, $this->view->curUserProfile->photo);
       
       
        $this->view->agency = new Agentstvo_Object($agency);

        $this->view->title       = Agentstvo_Seo_Pages::getValue($this->page, 'Редактировать профиль');
        $this->view->description = Agentstvo_Seo_Pages::getValue($this->page, 'Редактировать профиль');
        $this->view->keywords    = Agentstvo_Seo_Pages::getValue($this->page, 'Редактировать профиль');
        $redirect = $this->getRequest()->getParam('redirect_url');
        if ($redirect) {
            $this->_redirect($redirect);
        }
    }


    

}